# Document Object Model


## What is DOM?

The DOM stands for __Document Object Model__, it is an interface that allows developers to interact and control the elements of the webpage.

The Document Object Model is a model that defines HTML and XML documents as a __tree structure__, this has many methods that can be used to manipulate content, style, and structure
of the HTML page and is one of the most useful and unique tools of Javascript.

The DOM has a hierarchical structure. The lower elements are children of parent elements and side elements having the same parents are called siblings. These are called nodes (Root node, parent node, sibling node, child node).

![Dom tree image](./Images/dom.jpg "DOM tree structure")

---

## How does it help?

* We can update the contents and layout of the webpage without refreshing it.

* We can add, remove and delete the elements, attributes, and style.

---

## How to access it?

* There are five major ways to access elements in DOM.
    * getElementById()
    * getElementByClassName()
    * getElementByTagName()
    * querySelector()
    * querySelectorAll()

### Lets consider the following example.


```html
<!DOCTYPE html>
<html lang="en">
<head>
    <title>DOM</title>
</head>
<body>
    <h1 class = "heading" id = "Main heading">Hello World</h1>
    <div class="container">
        <h2>An unordered HTML list</h2>

        <ul>
            <li class="list-items" tag = "list-items">
                <span>Hot</span>
                Coffee
            </li>
            <li class="list-items" tag = "list-items">Tea</li>
            <li class="list-items" tag = "list-items">Milk</li>
        </ul>
        <div></div>
    </div>
</body>
</html>
```

## getElementById()

```javascript
let heading = document.getElementById("Main heading");
console.log(heading.textContent);
```
This prints the output **Hello World**.


## getElementByClassName()

```javascript
let list = document.getElementsByClassName("list-items");
for([key] of Object.keys(list)){
    console.log(list[key].textContent)
}
```
This method returns an array of elements with the className.


## getElementByTagName()
```javascript
let list = document.getElementsByTagName("li");
console.log(list);
```
This method returns an array of elements with tag 'li'.


## querySelector()
```javascript
let container = document.querySelector('div');
console.log(container);
```
This method returns the first occurred element which matches the argument.


## querySelectorAll()
```javascript
let container = document.querySelectorAll('div')
console.log(container)
```
This method returns all the elements which match the passed argument.

* We can also manipulate the elements by accessing them.

```javascript
let heading = document.getElementById("Main heading");
heading.textContent = "Modified text";
heading.style.color = "red";
```

* We can add or remove attriutes to the existing elements.

```javascript
let heading = document.getElementById("Main heading");
heading.setAttribute('id',"test");
heading.removeAttribute('class');
```

* We can modify class attribute of an existing element.
```javascript
let heading = document.getElementById("Main heading");
heading.classList.add("test")
heading.classList.remove("test")
```

* We can create html elements.

```javascript
let ul = document.querySelector("ul")
let li = document.createElement("li")
li.textContent = "Cold Coffee"
ul.append(li)
```

* We can remove html elements.

```javascript
let heading = document.getElementById("Main heading");
heading.remove();
```

* We can remove attributes of html elements.

```javascript
let heading = document.getElementById("Main heading");
heading.removeAttribute("class");
```

---

## DOM traversing

* Parent node traversing:

```javascript
let ul = document.querySelector('ul')
console.log(ul.parentNode)
console.log(ul.parentElement)

//To get grandparent we have to chain the property.
console.log(ul.parentNode.parentNode)
console.log(ul.parentElement.parentElement.parentNode)

//While using parentElement if there are no elements above, it returns null.

let html = document.documentElement;
console.log(html.parentElement) // returns null
console.log(html.parentNode) // returns document node 
console.log(html.parentNode.parentNode) // and above root node nothing's there and hence it returns null.
```

* Child node traversing:

```javascript
let ul = document.querySelector('ul')
console.log(ul.childNodes) // returns a list of all nodes and the indentation is considered as a text node.
console.log(ul.firstChild)
console.log(ul.lastChild) // returns text node
console.log(ul.firstElementChild)
console.log(ul.lastElementChild) // returns actual element
```

* Sibling node traversal:

```javascript
let div = document.querySelector('div')
console.log(div.childNodes)

console.log(ul.nextSibling)
console.log(ul.previousSibling) // returns text node if there is space.

console.log(ul.nextElementSibling)
console.log(ul.previousElementSibling) // returns the actual element

```

---

## Helper methods

 Helper methods are the methods that are called within a method and the return value of a method is used by another function.

```javascript
function indexOfParent(el) {
    return [].indexOf.call(el.parentElement.children, el);
}
let ul = indexOfParent(document.querySelector('ul'))
console.log(ul)
```
Here the return value of the method querySelector() is used by the indexOfParent() function.


---

## References

* [freecodecamp](https://www.freecodecamp.org/news/whats-the-document-object-model-and-why-you-should-know-how-to-use-it-1a2d0bc5429d "freecodecamp")

* [codeacademy](https://www.codecademy.com/courses/introduction-to-javascript/lessons/functions/exercises/return-ii "codeacademy")

* [youtube](https://youtu.be/5fb2aPlgoys "youtube")

* [helper_functions](https://www.youtube.com/watch?v=Bt-fbCT7WFw "helper functions")